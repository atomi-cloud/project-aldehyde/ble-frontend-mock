import {RuleSetQuery} from "webpack";

let prodFileOpts: RuleSetQuery = {
	name: '[path][name].[ext]',
	publicPath: 'var~rimage.publicPath~',
	context: './src'
};

export {
	prodFileOpts
}
